class Deck
	attr_accessor :cards

	def initialize()
		descriptions = [
		["ACCIDENT",3,"ATTAQUE","millebornes_a-accident.svg.png"],
		["PANNE_ESS",3,"ATTAQUE","millebornes_a-panne.svg.png"],
		["CREVAISON",3,"ATTAQUE","millebornes_a-roue.svg.png"],
		["LIMIT_VIT",4,"ATTAQUE","millebornes_a-50.svg.png"],
		["FEU_ROUGE",5,"ATTAQUE","millebornes_a-feu.svg.png"],
		["REPARATION",6,"DEFENSE","millebornes_d-accident.svg.png"],
		["ESSENCE",6,"DEFENSE","millebornes_d-panne.svg.png"],
		["ROUE_SEC",6,"DEFENSE","millebornes_d-roue.svg.png"],
		["LIMIT_FIN",6,"DEFENSE","millebornes_d-50.svg.png"],
		["FEU_VERT",14,"DEFENSE","millebornes_d-feu.svg.png"],
		["AS_DU_VOLANT",1,"BOTTE","millebornes_0-accident.svg.png"],
		["INCREVABLE",1,"BOTTE","millebornes_0-roue.svg.png "],
		["CITERNE",1,"BOTTE","millebornes_0-panne.svg.png"],
		["PRIORITAIRE",1,"BOTTE","millebornes_0-50.svg.png"],
		["25",10,"DISTANCE","millebornes_k-025.svg.png"],
		["50",10,"DISTANCE","millebornes_k-050.svg.png"],
		["75",10,"DISTANCE","millebornes_k-075.svg.png"],
		["100",12,"DISTANCE","millebornes_k-100.svg.png"],
		["200",4,"DISTANCE","millebornes_k-200.svg.png"]]

		puts "New deck created..."
		@cards=[]

		#Generating deck :
		descriptions.each  do |description|
			for i in 1..description[1] do
				une_carte=Carte.new
				une_carte.libelle=description[0]
				une_carte.type=description[2]
				une_carte.url=description[3]
				add_card(une_carte)
			end
		end
		#calling self method "melange"
		melange
    end

	def add_card(card)
		@cards << card
	end

	def melange()
		puts "Cards shuffled..."
		@cards.shuffle!
	end

	def afficher_jeu()
		for i in 1..(@cards.count) do
			puts "#{@cards[i-1].libelle} -> #{@cards[i-1].state} : #{@cards[i-1].playable}"
		end
	end

	def pick_card()
		# point towards the last card (0)
		i=0
		while i<@cards.count && @cards[i].state != "FREE" do
			i=i+1
		end
		if (i==(@cards.count)) then
			puts "plus de carte !!!"
			# ICI, on va reseter les DROPPED -> FREE
			return false
		else
			@cards[i].state = "TAKEN"
			return @cards[i]
		end

	end

	def defausser(carte)
		carte.state = "DROPPED"
	end


end

class Player
	attr_accessor :name, :hand, :distance,  :blocks
	# state = "STOPPED","LIMITED","FREE"

	def initialize(*args)
		@hand = []
		@name = args[0]
		@distance=0
		@blocks= ["FEU_ROUGE"]
		for i in 1..6 do
			@hand << $mon_deck.pick_card()
		end
	end

	def play_card(card)
		card.state = "PLAYED"
		card.playable="NON"

		# carte Distance.
		if (card.libelle=="DISTANCE") then
			@distance=@distance+card.libelle.to_i
		end

	end

	def pioche()
		@hand << $mon_deck.pick_card()
	end

	def update_hand()
		playable=false
		nbcarte=0
		@hand.each do |card|
			playable = case
				when card.type=="ATTAQUE" then true
				when card.type=="BOTTE" then true
				when card.type=="DEFENSE" then false
				when card.type=="DISTANCE" then false
			end
 			playable = true if
				(@blocks.include?("CREVAISON") && card.libelle=="ROUE_SEC") or
				(@blocks.include?("PANNE_ESS") && card.libelle=="ESSENCE") or
				(@blocks.include?("ACCIDENT") && card.libelle=="REPARATION") or
				(@blocks.include?("FEU_ROUGE") && card.libelle=="FEU_VERT") or
				(@blocks.include?("LIMITED") && card.libelle=="25") 	or
				(@blocks.include?("LIMITED") && card.libelle=="50") or
				(@blocks.count==0 && une_carte.type=="DISTANCE")
			nbcarte += 1 if playable
			card.playable=playable
		end
		return nbcarte
	end

	def play(card)
		play_card(card)
		i=0
	@hand.each do |tcard|
			if tcard.libelle=card.libelle
				@hand.delete_at(i)
			end
			i=i+1
		end
		puts @hand
		# à revoir .... : @hand.delete_at(input.to_i-1)
	end

	def defausse(card)
		$mon_deck.defausser(card)
		#	à revoir ...@hand.delete_at(input.to_i-1)*/
	end

end

class Carte
	attr_accessor :libelle, :type, :state, :playable, :url
	#state = FREE / PLAYED / DROPPED / TAKEN
	def initialize()
		@state='FREE'
		@playable='YES'
	end
end

class Interface
	def initialize(shoes)
		@shoes = shoes
	end

	def afficher_main(player)
		i=0
		@shoes.app do
			flow do
				player.hand.each do |card|
					puts "#{i} - #{card.libelle} "
					@p = para "#{i} - #{card.libelle} "
					if (card.playable)
						@p.style size: 12, stroke: green, margin: 15, top: 160	+15*i
						@b = button "Play"
						@b.click {
							para "play: #{card.libelle}"
							player.play(card)
							player.pioche
							player.update_hand
							afficher_main(player)
						}
						fill green
					else
						@p.style size: 12, stroke: red, margin: 15, top: 160+15*i
						@b = button "Dismiss"
						@b	.click { para "dismiss: #{card.libelle}"
							player.defausse(card)
							player.pioche
							player.update_hand
							afficher_main(player)
						}
						fill red
					end
					@b.style left: 5+70*i, width: 70, top: 50+85
					rect(left:  5+70*i,      top:   45,         width: 70, height: 82)
					image  "./pics/#{card.url}", top:  50, left: 10+70*i, width: 60
					i=i+1
				end
			end
		end
	end

end
	# Initialisation
	$mon_deck = Deck.new()
	playerA = Player.new("Joueur A")
	playerCPU = Player.new("Computer")


Shoes.app do
	interface = Interface.new(self)
	# On commence à jouer
	playerA.pioche
	playerA.update_hand
	interface.afficher_main(playerA)
end
