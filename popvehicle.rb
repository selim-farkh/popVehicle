require 'state_machine'
require "highline/import"

class PopVehicle
  state_machine :initial => :parked do

    event :ignite do
      transition [:parked] => :idling
    end

    event :park do
      transition [:idling, :first_gear] => :parked
    end

    event :gear_up do
      transition [:idling] => :first_gear,
      [:first_gear] => :second_gear,
      [:second_gear] => :third_gear
    end

    event :gear_down do
      transition [:first_gear] => :idling,
      [:second_gear] => :first_gear,
      [:third_gear] => :second_gear
    end
    event :crash do
      transition [:first_gear, :second_gear, :third_gear] =>:crashed
    end
    event :repair do
      transition [:crashed] => :parked
    end
  end
end

vehicle = PopVehicle.new

while true do
    puts "Le véhicule est #{vehicle.state}"
    puts "et peut recevoir : #{vehicle.state_events}"
    input = ask "-> Action ?: "
    vehicle.send(input)
end

vehicleA = PopVehicle.new
vehicleB = PopVehicle.new

while true do
  puts ="Le véhicule A est #{vehicleA.state}"
  puts "Le vehicule B est #{vehicleB.state}"
  input = ask "-> Action pour A ?:"
  input = ask "-> Action pour B ?:"
  vehicleA.send(input)
  vehicleB.send(input)  
end
